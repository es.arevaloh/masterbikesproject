from django.contrib import admin
from django.db import models
from .models import *

# Register your models here.
class admMarca(admin.ModelAdmin):
    list_display = ['id', 'nombre']
    class Meta:
        model=Marca
admin.site.register(Marca,admMarca)

class admModelo(admin.ModelAdmin):
    list_display = ['id', 'nombre']
    class Meta:
        model=Modelo
admin.site.register(Modelo,admModelo)

class admBicicleta(admin.ModelAdmin):
    list_display = ['id', 'marca', 'modelo', 'color', 'aro', 'precioArriendo', 'precioVenta']
    class Meta:
        model=Bicicleta
admin.site.register(Bicicleta,admBicicleta)
