from django.db import models
from django.forms import fields
from django import forms

# Create your models here.
class Marca(models.Model):
    id = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=30)
    def __str__(self):
        return str(self.nombre)
    
class Modelo(models.Model):
    id = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=30)
    def __str__(self):
        return str(self.nombre)

class Bicicleta(models.Model):
    id = models.AutoField(primary_key=True)
    marca = models.ForeignKey(Marca,default=1 ,on_delete=models.CASCADE)
    modelo = models.ForeignKey(Modelo,default=1 , on_delete=models.CASCADE,)
    color = models.CharField(max_length=15)
    aro = models.PositiveIntegerField()
    precioArriendo = models.PositiveIntegerField()
    precioVenta = models.PositiveIntegerField()
    def __str__(self):
        return str(self.modelo)

class Refaccion(models.Model):
    id = models.AutoField(primary_key=True)
    marca = models.ForeignKey(Marca,default=1 , on_delete=models.CASCADE,)
    modelo = models.ForeignKey(Modelo,default=1 , on_delete=models.CASCADE,)
    preceioVenta = models.PositiveIntegerField()
