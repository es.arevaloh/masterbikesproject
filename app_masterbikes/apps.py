from django.apps import AppConfig


class AppMasterbikesConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'app_masterbikes'
