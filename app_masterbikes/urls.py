from django.urls import path,  include 
from .views import *

urlpatterns = [
    path('',home,name="home"),
    path('mantenedor',mantenedor,name="mantenedor"),
    path('addbike',addbike,name="addbike"),
    path('editbike/<id>/',editbike, name='editbike'),
    path('dell/<id>/',dell, name='dell'),
    path('stock',stock,name="stock"),

]

