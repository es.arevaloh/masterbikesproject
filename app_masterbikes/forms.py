from django import forms
from django.forms import fields
from app_masterbikes.models import *

class frmBicicleta(forms.ModelForm):
    class Meta:
        model = Bicicleta
        fields = ('marca','modelo','color','aro','precioArriendo','precioVenta')