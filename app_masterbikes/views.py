from .forms import *
from django.shortcuts import get_object_or_404, redirect, render
from .models import *

# Create your views here.
def home(request):
    return render(request,"app_masterbikes/home.html")

def mantenedor(request):
    bikes = Bicicleta.objects.all()
    contexto = {'bikes':bikes}
    return render(request,"app_masterbikes/mantenedor.html",contexto)

def addbike(request):
    frmBike = frmBicicleta(request.POST or None)
    contexto = {'frmBike' : frmBike}
    if request.method=="POST":
        data = request.POST
        bike = frmBicicleta(data)
        if bike.is_valid():
            bike.save()
            bike.clean()
            contexto = {'mensaje' : 'Bicicleta agregada Correctamente'}
            return render(request,"app_masterbikes/addbike.html",contexto)
    return render(request,"app_masterbikes/addbike.html", contexto)

def editbike(request, id):
    bike = get_object_or_404(Bicicleta, id = id)
    frmBike=frmBicicleta(instance=bike)
    contexto = {'frmBike' : frmBike}
    if request.method == "POST": #lo que se traduce en si el boton submit fue activado
        #producto.fields['id_categoria'].disabled=False
        data=request.POST
        frmBike=frmBicicleta(data,instance=bike)
        if frmBike.is_valid():
            frmBike.save()
            frmBike.clean()
            return redirect(to="mantenedor") 
    return render(request,"app_masterbikes/addbike.html", contexto)

def dell(request, id):
    bike = get_object_or_404(Bicicleta, id = id)
    contexto = {'bike' : bike}
    if request.method == "POST":
        bike.delete()
        contexto= {
            'mensaje' : 'Bicicleta Eliminada Exitosamente'
        }
        return render(request, 'app_masterbikes/dell.html',contexto)
    return render(request, 'app_masterbikes/dell.html',contexto)

def stock(request):
    bikes = Bicicleta.objects.all()
    contexto = {'bikes':bikes}
    return render(request,"app_masterbikes/stock.html",contexto)
